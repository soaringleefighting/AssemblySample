/******************************************************************
/*    KunPeng Technology CO. LTD
/*    2001-2020 Copyright reversed.
/*    @File				:	gradientImage.c
/*    @Description		:   this file is gradient implementation for AssemblySample Project.
/*	  @Feature			:	1.梯度图计算的C实现。
/*    @Author			:	lipeng
/*    @Revison History	:
/*		1. Date			: 2020.9.5
/*		   Author		: lipeng
/*		   Modification	: create the file
/*	  @Version	: 1.0.0
/********************************************************************/
#include "gradientImage.h"

// 计算当前图像的梯度图
int generateGradientImage(unsigned char *Src, unsigned char *Dst,
	int Width, int Height, int Stride)
{
	unsigned int gradX = 0;
	unsigned int gradY = 0;
	unsigned char * dstImage = NULL;

	// 1. 求取梯度图像
	memset(Dst, 0, Width * sizeof(unsigned int));         // 第一行都为0
	for (int y = 1; y < Height; y++)
	{
		dstImage = Dst + y * Width + 1;
		dstImage[-1] = 0;									// 第一列为0
		for (int x = 1; x < Width; x++)
		{
			gradX = abs(Src[y * Stride + x] - Src[y * Stride + x - 1]);
			gradY = abs(Src[y * Stride + x] - Src[(y - 1) * Stride + x]);
			dstImage[x - 1] = (gradX + gradY) >> 1;
		}
	}

	return 0;
}