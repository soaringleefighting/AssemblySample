;----------------------------------------------------------------------------
;    KunPeng Technology CO. LTD
;    2001-2020 Copyright reversed.
;    @File				:	scm_gradientImage_x86_32.asm
;    @Description		:   this file is x86 assembly code for gradient image.
;	 @Feature			:	1. support sse2 for x86 platform.
;    @Author			:	lipeng
;    @Revison History	:
;		1. Date			: 2020.8.18
;		   Author		: lipeng
;		   Modification	: create the file
;	 @Version	: 1.0.0
;----------------------------------------------------------------------------

%include "x86util.asm"
%include "x86inc.asm"


%macro GRADIENT_CORE 4				
		punpcklbw %2, %4					; byte--->word
		punpcklbw %3, %4					; %4 default is m7
		
		psubw	  %2, %3					; gradX
		psubw	  %1, %3 					; gradY

		pabsw     %2, %2					; abs(gradX)
		pabsw     %1, %1					; abs(gradY)
	
		paddw	  %1, %2					; abs(gradX)+abs(gradY)

		psraw	  %1, 1						; >>1	
		packuswb  %1, %1					; word--->byte
%endmacro

section .text

;----------------------------------------------------------------------------
; int generateGradientImage(unsigned char *Src, unsigned char *Dst, 
;							int Width, int Height, int Stride)
;----------------------------------------------------------------------------
;    @Funtion Name		:  x264_generateGradientImage_sse2
;    @Description		:  calculate gradient image for current image
;    @Input para		:  unsigned char *Src	: source image				r0
;						   int Width			: width of image			r2
;						   int Height			: height of image			r3
;						   int Stride			: stride of image			r4
;	 @Output para		:  unsigned int *Dst	: dstination gradient image	r1
;    @Return			:  0: Success  -1: Failure
;    @Author			:  lipeng
;	 @Others			:  将图像划分成很多8x4的块，对块中的每一个像素进行梯度计算，
;						  一次处理8个像素点，先处理列再处理行，特别注意边界上像素的
;						  处理。
;    @Revison History	:
;		1. Date			: 2020.8.18
;		   Author		: lipeng
;		   Modification	: create the function	
;	 @使用要求			: 处理的图像宽度要求是8的倍数，高度要求是4的倍数
;==============================================================================
INIT_XMM sse4

ALIGN 16

cglobal generateGradientImage, 5, 6, 8, 0x20, src, dst, width, height, stride, heightb
		movzx	strideq,	stridew			; 高位置零
		mov		heightbq,	heightq			; 保存heightq
		mov		r8m,		srcq			; 保存srcq
		mov		r9m,		dstq			; 保存dstq
		mov     r10m,		widthq			; 保存widthq
		
.loop_row:

.main_loop:
		; 主处理函数
		; 1.取出8x4块的原始像素值
		sub		srcq,		strideq			; 上一行像素位置
		cmp		heightq,	heightbq
		je		.start_1
		movq    m0,			[srcq]			; 取出上一行8个像素点
.start_1:
		add		srcq,		strideq
		movq	m1,			[srcq]			; 取出第一行8个像素点
		add		srcq,		strideq
		movq	m2,			[srcq]			; 取出第二行8个像素点
		add		srcq,		strideq
		movq	m3,			[srcq]			; 取出第三行8个像素点
		add		srcq,		strideq
		movq	m4,			[srcq]			; 取出第四行8个像素点
		add		srcq,		strideq

		; 左移一个像素点
		cmp		widthq,  r10m
		je     .left_1
		
		; 非第一列的块
		sub		srcq,		strideq			; 回到第一行位置
		sub		srcq,		strideq
		sub		srcq,		strideq
		sub		srcq,		strideq
		
		sub		srcq,		1
		movq    m5,			[srcq]			; 取出左移后的第一行8个像素
		add		srcq,		strideq
		movq	m6,			[srcq]			; 取出左移后的第二行8个像素
		add		srcq,		strideq
		jmp		.next_1
.left_1:		
		; 考虑到边界上内存读取越界风险(第一列)
		psllq	m5,			m1,		8
		psllq	m6,			m2,		8
		
.next_1:	
		; 2. gradX和gradY的计算
		; 特别注意:加减运算溢出问题
		pxor	m7, m7						; m7: 0 0 0 0 0 0 0 0
		punpcklbw m0, m7
		
		; 2.1 第一行
		GRADIENT_CORE m0, m5, m1, m7		; 梯度计算

		; 2.2 第二行
		GRADIENT_CORE m1, m6, m2, m7		; 梯度计算

		; 左移一个像素，此处复用m5和m6
		cmp		widthq,  r10m
		je     .left_2
		
		movq	m5,			[srcq]			; 取出左移后的第三行8个像素
		add		srcq,		strideq
		movq	m6,			[srcq]			; 取出左移后的第四行8个像素 
		add		srcq,		strideq
		
		add		srcq,		1
		jmp     .next_2
.left_2:		
		; 考虑到边界上内存读取越界风险(第一列)
		psllq	m5,			m3,		8
		psllq	m6,			m4,		8
		
.next_2:		
		; 2.3 第三行
		GRADIENT_CORE m2, m5, m3, m7

		; 2.4 第四行
		GRADIENT_CORE m3, m6, m4, m7

		; 3.存储梯度值到dst中
		cmp		heightq,	heightbq
		jne		.end_1
		pand	m0,			m7		; 梯度图第一行置零
.end_1:
		cmp		widthq,  r10m
		jne     .end_2
		
		; 梯度图第一列置零
		mov     r7m,  r2d
		mov		r2d,  0xFFFFFF00
		pinsrd	m7,	 r2d, 0			; 低32位置为FFFFFF00
		mov		r2,  0xFFFFFFFF
		pinsrd	m7,	 r2d, 1			; 高3l2位置为FFFFFFFF
		mov     r2d,  r7m
		; 将m0/m1/m2/m3的低8位置零
		pand		m0, m7
		pand		m1, m7
		pand		m2, m7
		pand		m3, m7
.end_2:
		movq	[dstq], m0
		add		dstq, r10m
		movq	[dstq], m1
		add		dstq, r10m
		movq	[dstq], m2
		add		dstq, r10m
		movq	[dstq], m3
		add		dstq, r10m

.loop_4:
		; 4.循环处理
		; 处理列
		sub		heightq, 4
		jg		.main_loop
		
		; 处理行
		mov		heightq, heightbq	; 恢复height

		; 恢复src
		mov		srcq,   r8m
		add		srcq,	8
		mov		r8m,	srcq

		; 恢复dst
		mov		dstq, r9m
		add     dstq, 8
		mov     r9m,  dstq

		sub		widthq, 8
		jg		.loop_row
RET

