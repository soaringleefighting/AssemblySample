/******************************************************************/
/*    KunPeng Technology CO. LTD
/*    2001-2020 Copyright reversed.
/*    @File				:	scm_gradientImage_x86_intrin_sse4.c
/*    @Description		:   this file is x86 intrinsic code for gradient image calculation.
/*	  @Feature			:	1. support sse4 intrinsic for x86 platform.
/*    @Author			:	lipeng
/*    @Revison History	:
/*		1. Date			: 2020.9.4
/*		   Author		: lipeng
/*		   Modification	: create the file
/*	 @Version	: 1.0.0
/*******************************************************************/

#include <mmintrin.h>		/* MMX */
#include <emmintrin.h>		/* SSE2 */
#include <tmmintrin.h>		/* SSSE3 */
#include <smmintrin.h>		/* SSE4.1 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/******************************************************************
/*    KunPeng Technology CO. LTD
/*    2001-2020 Copyright reversed.
/*    @Funtion Name		:  scm_generateGradientImage_sse4
/*    @Description		:  calculate gradient image for current image
/*    @Input para		:  unsigned char *Src	: source image
/*						   int Width			: width of image
/*						   int Height			: height of image
/*						   int Stride			: stride of image
/*	  @Output para		:  unsigned int *Dst	: dstination gradient image
/*    @Return			:  0: Success  -1: Failure
/*    @Author			:  lipeng
/*    @Revison History	:
/*		1. Date			: 2020.9.5
/*		   Author		: lipeng
/*		   Modification	: create the function
/********************************************************************/
int scm_generateGradientImage_intrinsic_sse4(unsigned char *Src, unsigned char *Dst, 
							int Width, int Height, int Stride)
{
	__m128i last_line, firstline, second_line, third_line, four_line;
	__m128i first_line_lt, second_line_lt, third_line_lt, four_line_lt;
	
	unsigned char *Src_in = NULL;
	unsigned char *Dst_in = NULL;

	// 初始化
	memset( Dst, 0, Width * sizeof(unsigned int) );         // 第一行都为0

	for (int y = 1; y < Height; y+=4)
	{
		Src_in = Src + y * Stride;
		Dst_in = Dst + y * Width;

		for (int x = 0; x < Width; x+=8)
		{
			
			// 1.加载原始像素值
			last_line = _mm_loadl_epi64((__m128i const*)(Src_in - Stride));			// 上一行
			firstline = _mm_loadl_epi64((__m128i const*)Src_in);					// 取出第一行8个像素点
			second_line = _mm_loadl_epi64((__m128i const*)(Src_in + Stride));		// 第二行
			third_line = _mm_loadl_epi64((__m128i const*)(Src_in + 2 * Stride));	// 第三行
			four_line = _mm_loadl_epi64((__m128i const*)(Src_in + 3 * Stride));		// 第四行

			// 2.左移一个像素点
			if (x == 0) /*  考虑边界上内存读取越界的风险（第一列） */
			{
				first_line_lt = _mm_slli_epi64(firstline, 8);
				second_line_lt = _mm_slli_epi64(second_line, 8);
				third_line_lt = _mm_slli_epi64(third_line, 8);
				four_line_lt = _mm_slli_epi64(four_line, 8);
			}
			else
			{
				first_line_lt = _mm_loadl_epi64((__m128i const*)(Src_in - 1));
				second_line_lt = _mm_loadl_epi64((__m128i const*)(Src_in - 1 + Stride));	// 第二行
				third_line_lt = _mm_loadl_epi64((__m128i const*)(Src_in - 1 + 2 * Stride));	// 第三行
				four_line_lt = _mm_loadl_epi64((__m128i const*)(Src_in - 1 + 3 * Stride));	// 第四行
			}

			// 3.8位扩展到16位
			__m128i zero = _mm_setzero_si128();

			last_line = _mm_unpacklo_epi8(last_line, zero);
			firstline = _mm_unpacklo_epi8(firstline, zero);
			second_line = _mm_unpacklo_epi8(second_line, zero);
			third_line = _mm_unpacklo_epi8(third_line, zero);
			four_line = _mm_unpacklo_epi8(four_line, zero);

			first_line_lt = _mm_unpacklo_epi8(first_line_lt, zero);
			second_line_lt = _mm_unpacklo_epi8(second_line_lt, zero);
			third_line_lt = _mm_unpacklo_epi8(third_line_lt, zero);
			four_line_lt = _mm_unpacklo_epi8(four_line_lt, zero);

			// 4.第一行梯度计算
			__m128i one = { 1 };
			last_line	  = _mm_sub_epi16(last_line, firstline);			// gradY
			first_line_lt = _mm_sub_epi16(first_line_lt, firstline);		// gradX
			last_line	  = _mm_abs_epi16(last_line);						// abs(gradY)
			first_line_lt = _mm_abs_epi16(first_line_lt);					// abs(gradX)
			first_line_lt = _mm_add_epi16(first_line_lt, last_line);		// abs(gradX)+abs(gradY)
			first_line_lt = _mm_sra_epi16(first_line_lt, one);				// >>1
			first_line_lt = _mm_packus_epi16(first_line_lt, first_line_lt);

			// 5.第二行梯度计算
			firstline = _mm_sub_epi16(firstline, second_line);				// gradY
			second_line_lt = _mm_sub_epi16(second_line_lt, second_line);	// gradX
			second_line_lt = _mm_abs_epi16(second_line_lt);					// abs(gradY)
			firstline = _mm_abs_epi16(firstline);							// abs(gradX)
			second_line_lt = _mm_add_epi16(firstline, second_line_lt);		// abs(gradX)+abs(gradY)
			second_line_lt = _mm_sra_epi16(second_line_lt, one);			// >>1
			second_line_lt = _mm_packus_epi16(second_line_lt, second_line_lt);

			// 6.第三行梯度计算
			second_line = _mm_sub_epi16(second_line, third_line);			// gradY
			third_line_lt = _mm_sub_epi16(third_line_lt, third_line);		// gradX
			third_line_lt = _mm_abs_epi16(third_line_lt);					// abs(gradY)
			second_line = _mm_abs_epi16(second_line);						// abs(gradX)
			third_line_lt = _mm_add_epi16(second_line, third_line_lt);		// abs(gradX)+abs(gradY)
			third_line_lt = _mm_sra_epi16(third_line_lt, one);				// >>1
			third_line_lt = _mm_packus_epi16(third_line_lt, third_line_lt);

			// 7.第四行梯度计算
			third_line = _mm_sub_epi16(third_line, four_line);				// gradY
			four_line_lt = _mm_sub_epi16(four_line_lt, four_line);			// gradX
			four_line_lt = _mm_abs_epi16(four_line_lt);						// abs(gradY)
			third_line = _mm_abs_epi16(third_line);							// abs(gradX)
			four_line_lt = _mm_add_epi16(third_line, four_line_lt);			// abs(gradX)+abs(gradY)
			four_line_lt = _mm_sra_epi16(four_line_lt, one);				// >>1
			four_line_lt = _mm_packus_epi16(four_line_lt, four_line_lt);

			// 第一列为0
			if (x == 0) 
			{
				zero = _mm_insert_epi32(_mm_insert_epi32(zero, 0xFFFFFF00, 0), 0xFFFFFFFF, 1);
				first_line_lt = _mm_and_si128(first_line_lt, zero);
				second_line_lt = _mm_and_si128(second_line_lt, zero);
				third_line_lt = _mm_and_si128(third_line_lt, zero);
				four_line_lt = _mm_and_si128(four_line_lt, zero);
			}
			// 8.存储
			_mm_storel_epi64((__m128i*)Dst_in, first_line_lt);
			_mm_storel_epi64((__m128i*)(Dst_in + Width), second_line_lt);
			_mm_storel_epi64((__m128i*)(Dst_in + Width * 2), third_line_lt);
			_mm_storel_epi64((__m128i*)(Dst_in + Width * 3), four_line_lt);

			Src_in += 8;
			Dst_in += 8;
		}
	}

	return 0;
}
