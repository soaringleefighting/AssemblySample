/******************************************************************
/*    Kunpeng Technology CO. LTD
/*    2010-2020 Copyright reversed
/*    @File			:	gradientImage.h
/*    @Description	:	this file is header file of gradientImage.c
/*    @Author		:	lipeng
/*    @Modified		:	2020.9.5	Created
/********************************************************************/

#ifndef ASSEMBLYSAMPLE_GRADIENTIMAGE_H
#define ASSEMBLYSAMPLE_GRADIENTIMAGE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* ��ຯ������*/
#if ARCH_X86
#if ARCH_X86_64   /* X86�ܹ�64λ */
extern int x264_generateGradientImage_avx2(unsigned char *Src, unsigned char *Dst, 
											int Width, int Height, int Stride); //x86 �����
#else			 /* X86�ܹ�32λ */
extern int x264_generateGradientImage_sse4(unsigned char *Src, unsigned char *Dst, 
											int Width, int Height, int Stride); //x86 �����
#endif

extern int scm_generateGradientImage_intrinsic_sse4(unsigned char *Src, unsigned char *Dst,
	int Width, int Height, int Stride);	// x86 Intrinsic SSE4
#endif

/******************************************************************
/*    KunPeng Technology CO. LTD
/*    2001-2020 Copyright reversed.
/*    @Funtion Name		:  generateGradientImage
/*    @Description		:  calculate gradient image for current image
/*    @Input para		:  unsigned char *Src	: source image
/*						   int Width			: width of image
/*						   int Height			: height of image
/*						   int Stride			: stride of image
/*	  @Output para		:  unsigned int *Dst	: dstination gradient image
/*    @Return			:  0: Success  -1: Failure
/*    @Author			:  lipeng
/*    @Revison History	:
/*		1. Date			: 2020.8.17
/*		   Author		: lipeng
/*		   Modification	: create the function
/********************************************************************/
int generateGradientImage(unsigned char *Src, unsigned char *Dst,
	int Width, int Height, int Stride);

#endif