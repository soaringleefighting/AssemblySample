# AssemblySample
This is a sample project for demonstrating how to write assembly code simply.

## Sample功能和搭建目的
1、支持x86 assembly, inline, instrinsic的demo及编译

2、支持ARM assembly, inline, intrinsic的demo及编译

3、通过该Sample旨在快速入门X86和ARM汇编程序编写方法和使用方法。

## Linux/ARM编译方法
（1）编译64位程序： make或者make platform=x86_64

（2）编译32位程序：make platform=x86_32
