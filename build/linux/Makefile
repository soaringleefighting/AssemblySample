#########################################################################
## Function: makefile for AssemblySample project
## Platform: linux
## Author: 	 lipeng
## Modified: 2020.9.1 created
#########################################################################

platform ?= x86_64
PUREC ?= 0
DEBUG ?= 0

#包含编译配置(系统或架构相关宏以及编译链接选项等配置)
include config.mk

#指定后缀名和伪目标
.SUFFIXES: .c,.o,.cpp,.S,.s  
.PHONY: all,clean

#设置相关路径
vpath %.h ./../../src
ROOTSRC = ./../../src
DEMOSRC = ./../../demo
INCLUDES = ./../../demo

#设置工具的相关参数
BIN_TARGET = ./../../bin/gradient_demo

#设置编译的相关参数
CFLAGS := -Wall -std=c99 $(DEBUG_FLAGS) -I$(INCLUDES)
CFLAGS += $(EXTRA_CFLAGS) 

CXXFLAGS := -Wall  

LDFLAGS := -Wall
LDFLAGS += $(EXTRA_LFLAGS)
 
ARFLAGS := -crus

ASMFLAGS += $(EXTRA_AFLAGS)

#以下获取OBJS文件的方式适用于含有少量文件的编译
SRCS = $(DEMOSRC)/os_time_sdk.c			      
SRCS += $(DEMOSRC)/demo.c  \
		$(ROOTSRC)/gradientImage.c

ifeq ($(PUREC), 0)
#X86 32位汇编
ifeq ($(platform), x86_32)
SRCS += $(ROOTSRC)/x86/scm_gradientImage_x86_32.asm \
	$(ROOTSRC)/x86/scm_gradientImage_x86_intrin_sse4.c
endif

#X86 64位汇编
ifeq ($(platform), x86_64)
SRCS += $(ROOTSRC)/x86/scm_gradientImage_x86_64.asm \
	$(ROOTSRC)/x86/scm_gradientImage_x86_intrin_sse4.c
endif

# ARM32位NEON汇编
ifeq ($(platform), arm32)

endif

# AARCH64 NEON汇编
ifeq ($(platform), arm64)

endif
endif
	   
OBJS = $(patsubst %.asm,%.o, $(patsubst %.cpp,%.o, $(patsubst %.c,%.o,$(SRCS))))

#编译规则
all: clean $(BIN_TARGET)
$(BIN_TARGET):	$(OBJS)
#	$(error $(SRCS))
	$(CC) -o $@ $(OBJS) $(LDFLAGS)

%.o: %.c
	$(CC) -c $< $(CFLAGS) -o $@
%.o: %.cpp
	$(CXX) -c $< $(CXXFLAGS) -o $@
		
%.o:%.asm
	$(ASM) $< $(ASMFLAGS) -o $@	
%.o:%.S
	$(ASM) $< $(ASMFLAGS) -c -o $@
%.o:%.s
	$(ASM) $< $(ASMFLAGS) -c -o $@	
		
clean:
	-rm	$(OBJS)
	-rm $(BIN_TARGET)
