/******************************************************************
/*    KunPeng Technology CO. LTD
/*    2001-2020 Copyright reversed.
/*    @File				:	demo.c
/*    @Description		:   this file is test demo for AssemblySample Project.
/*	  @Feature			:	1.梯度图计算SSE2优化; 2.梯度图计算AVX2优化。
/*    @Author			:	lipeng
/*    @Revison History	:
/*		1. Date			: 2020.9.1
/*		   Author		: lipeng
/*		   Modification	: create the file
/*	  @Version	: 1.0.0
/********************************************************************/
#include <assert.h>

#include "os_time_sdk.h"
#include "../src/gradientImage.h"

/* 优化宏定义 */
//#define  ARCH_ARM	   	   (0)		/* 0表示纯C，1表示arm neon优化 */
//#define  ARCH_AARCH64	   (0)		/* 0表示纯C，1表示arm neon优化 */
//#define  ARCH_X86	   	   (1)		/* 1:开启x86 assembly  0:不开启x86 assembly */
// NOTICE: ARCH_MACROS defined by makefile

#define    X86_ASM			(0)		/* X86纯汇编*/
#define    X86_INTRINSIC	(1)		/* X86 Intrisic */
#define    X86_INLINE		(0)		/* X86 Inline */


/* main: test demo */
int main(int argc, char** argv)
{
	unsigned char* y		 = NULL;
	unsigned char* gradImage = NULL;
	FILE*	fp_input		 = NULL;
	FILE*	fp_output		 = NULL;

	int width				= 0;
	int height				= 0;
	int filelen				= 0;
	int frames_no			= 0;
	int picturesize			= 0;
	int framenum			= 0;
	int framenum_end		= 0;

	os_timer t_os_timer		  = { 0 };
	os_timer t_os_timer1	  = { 0 };
	double   time_count		  = 0.0;
	double   time_count_c	  = 0.0;
	double   time_avg		  = 0.0;
	double   time_avg_c		  = 0.0;

	if (argc < 6)
	{
		printf("Usage: demo.exe input.yuv output.yuv  width height framenum \n\n");
		return -1;
	}

	width			= atoi(argv[3]);
	height			= atoi(argv[4]);
	framenum_end	= atoi(argv[5]);

	fp_input = fopen(argv[1], "rb");
	if (NULL == fp_input)
	{
		printf("open %s fail!\n", argv[1]);
		return -1;
	}

	fp_output = fopen(argv[2], "wb");
	if (NULL == fp_output)
	{
		printf("open %s fail!\n", argv[2]);
		return -1;
	}

	picturesize = width*height * 3 / 2;
	fseek(fp_input, 0, SEEK_END);
	filelen = ftell(fp_input);
	frames_no = filelen / picturesize;

	fseek(fp_input, 0, SEEK_SET);
	/* 只处理亮度分量 */
	y = (unsigned char*)malloc(width*height*sizeof(unsigned char));
	if (NULL == y)
	{
		printf("malloc y fail!\n");
		return -1;
	}

	gradImage = (unsigned char*)malloc(width*height*sizeof(unsigned char)); // 为梯度图像开辟内存空间
	if (NULL == gradImage)
	{
		printf("malloc gradImage fail!\n");
		return -1;
	}

	os_sdk_inittimer(&t_os_timer);
	os_sdk_inittimer(&t_os_timer1);

	while (fread(y, 1, width * height, fp_input) == width * height)
	{
		framenum++;

#if ARCH_X86 || ARCH_ARM || ARCH_AARCH64
		os_sdk_starttimer(&t_os_timer);
		
/* X86架构64位优化*/		
#if ARCH_X86_64
#if X86_ASM
		printf("x86_asm AVX2 test ...");
		x264_generateGradientImage_avx2(y, gradImage, width, height, width);
#endif

#if X86_INTRINSIC
		printf("x86_intrinsic SSE4 test ...");
		scm_generateGradientImage_intrinsic_sse4(y, gradImage, width, height, width);
#endif
#else /* X86架构32位优化*/	
#if X86_ASM
		printf("x86_asm SSE4 test ...");
		x264_generateGradientImage_sse4(y, gradImage, width, height, width);
#endif
#if X86_INTRINSIC
		printf("x86_intrinsic SSE4 test ...");
		scm_generateGradientImage_intrinsic_sse4(y, gradImage, width, height, width);
#endif
#endif

/* ARM架构32位优化*/	
#if ARCH_ARM
		printf("ARM NEON test ...");

#endif

/* ARM架构64位优化*/	
#if ARCH_AARCH64
		printf("AARCH64 NEON test ...");

#endif

		time_count = os_sdk_stoptimer(&t_os_timer);
		time_avg += time_count;

/* 纯C,作为对比测试 */
		os_sdk_starttimer(&t_os_timer1);
		generateGradientImage(y, gradImage, width, height, width);
		time_count_c = os_sdk_stoptimer(&t_os_timer1);
		time_avg_c += time_count_c;

#else /* 纯C */
		os_sdk_starttimer(&t_os_timer);
		generateGradientImage(y, gradImage, width, height, width);
		time_count_c = os_sdk_stoptimer(&t_os_timer);
		time_avg_c += time_count_c;
#endif

		printf("[gradient_demo] framenum: %d, purec consumed time: %f ms, sse2/neon consumed time: %f ms\n", 
			framenum, time_count_c, time_count);

		fwrite(gradImage, 1, width*height, fp_output);

		fseek(fp_input, (width*height) / 2, SEEK_CUR); // 跳过色度分量的处理

		if (framenum >= framenum_end)
		{
			break;
		}
	}

	time_avg_c = time_avg_c / framenum;
	time_avg = time_avg / framenum;

	printf("[gradient demo] %d frames completed!! purec average time: %f ms, sse2/neon consumed time: %f ms\n",
			framenum, time_avg_c, time_avg);

	if (y != NULL)
	{
		free(y);
		y = NULL;
	}

	if (gradImage != NULL)
	{
		free(gradImage);
		gradImage = NULL;
	}

	fclose(fp_input);
	fclose(fp_output);

#if _WIN32
	system("pause");
#endif
	return 0;
}